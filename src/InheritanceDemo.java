package ee.bcs.programming;

public class InheritanceDemo {

    interface TwoWheeled {

    }

    interface FourWheeled {

        double getFrontWheelRadius();

        double getRearWheelRadius();

        default double getFrontWheelCircumference() {
            return Math.PI * 2 * getFrontWheelRadius();
        }

    }

    interface Fast {

    }

    static class Vehicle {

    }

    static class Car extends Vehicle {
        private String manufacturer;
        private String model;

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

//        public Car() {
//
//        }

        public Car(String manufacturer, String model) {
            this.manufacturer = manufacturer;
            this.model = model;
        }
    }

    static class Opel extends Car implements FourWheeled, Fast {

        private double frontWheelRadius = 14;
        private double rearWheelRadius = 14;

        public Opel(String manufacturer, String model) {
            super(manufacturer, model);
        }

        @Override
        public double getFrontWheelRadius() {
            return frontWheelRadius;
        }

        @Override
        public double getRearWheelRadius() {
            return rearWheelRadius;
        }
    }

    static class Ferrari extends Car {

        private boolean isRacingCar = false;

        public Ferrari(String manufacturer, String model, boolean isRacingCar) {
            super(manufacturer, model);
            this.isRacingCar = isRacingCar;
        }
    }

    public static void main(String[] args) {
        Opel opel1 = new Opel("Opel", "Corsa");
        opel1.getFrontWheelCircumference();

//        Car car1 = new Opel();
        Car car1 = new Ferrari("Ferrari", "F355", false);
//        Opel opel2 = new Car(); Nii ei tohi!


    }

    static class Product {
        private String title;
        private int count;

        public String getTitle() {
            return title;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public Product(String title, int count) {
            this.title = title;
            this.count = count;
        }
    }
}

