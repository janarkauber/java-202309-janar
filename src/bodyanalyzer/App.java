package bodyanalyzer;

public class App {

    // Võta inimese pikkus cm OK
    // Võta inimese kehakaal kg OK
    // Konverteeri pikkus meetriteks
    // Arvuta kehamassiindeks: kehakaal / (pikkus meetrites * pikkus meetrites)
    // Tagasta tulemus
    // Tee STAATILINE funktsioon
//    public static double calculateBmi(int height, double weight) {
//        double heightInM = height / 100.0; // 180 / 100 -> 1 AGA 180 / 100.0 -> 1.8
//        // 23.148148 -> 23.15
//        // 23.148148 * 100 -> 2314.8148
//        // Math.round(2314.8148) --> 2315.0
//        // 2315.0 / 100 = 23.15
//        double result = weight / (heightInM * heightInM);
//        return Math.round(result * 100) / 100.0;
//    }

    public static void main(String[] args) {
//        System.out.println(calculateBmi(180, 75));
        int height = 180;
        double weight = 75.0;

        double bmi = calculateBmi(height, weight);
        System.out.println(bmi);
        System.out.println(deriveBmiText(bmi));

    }

    public static double calculateBmi(int height, double weight) {
        double heightInM = height / 100.0; // 180 / 100 -> 1 AGA 180 / 100.0 -> 1.8
        // 23.148148 -> 23.15
        // 23.148148 * 100 -> 2314.8148
        // Math.round(2314.8148) --> 2315.0
        // 2315.0 / 100 = 23.15
        double result = weight / (heightInM * heightInM);
        return Math.round(result * 100) / 100.0;
    }

    public static String deriveBmiText(double bmi) {
        if (bmi < 16) {
            return "Tervisele ohtlik alakaal";
        } else if (bmi < 19) {
            return "Alakaal";
        } else if (bmi < 25) {
            return "Normaalkaal";
        } else if (bmi < 30) {
            return "Ülekaal";
        } else if (bmi < 35) {
            return "Rasvumine";
        } else if (bmi < 40) {
            return "Tugev rasvumine";
        } else {
            return "Tervisele ohtlik rasvumine";
        }
    }
}

