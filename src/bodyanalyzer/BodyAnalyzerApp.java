package bodyanalyzer;

public class BodyAnalyzerApp {

    public static void main(String[] args) {
        BodyInfo b1 = new BodyInfo("Tambet", 86.6, 182);
        BodyInfo b2 = new BodyInfo("Triin", 67.8, 174);
        BodyInfo b3 = new BodyInfo("Teet", 97.8, 184);
        b3.setWeight(91.3);
        BodyInfo b4 = new BodyInfo();
        b4.setName("Kaarel");
        b4.setWeight(78);
        b4.setHeight(185);

        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);
        System.out.println(b4);
//        System.out.println(b1.getBmi());
//        System.out.println(b2.getBmi());
//        System.out.println(b3.getBmi());
    }
}

