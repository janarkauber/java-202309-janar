public class ArrayDemo {
    public static void main(String[] args) {
        int[] myArrayOfints  = new int[3];
        myArrayOfints[2] = 11;
        System.out.println(myArrayOfints[2]);
        myArrayOfints[2] = 12;
        System.out.println(myArrayOfints[2]);
        int[] myNumbers = {4, 5, 676, 9, 3};
        System.out.println(myNumbers[2]);

        String [] names = {"Pille", "Mari", "Mati"};
        System.out.println(names [2]);
        System.out.println("Massivi pikkus: " + names.length);
        System.out.println(names[names.length - 1]);

        String [] suits = {"Clubs", "Diamonds", "Hearts", "Spades"};

//        int[][] table = new int[3][2];
////
//        System.out.println(table[0].length);

//        String[][] table = new String[3][2];
        //Nimi Vanus - element indeksiga 0
        //Mari 23 indeks 1
        // Malle 67 indeks 2
        String[][] table = {
                { "Nimi", "Vanus" },
                { "Mari", "23" },
                {"Malle", "67" }

        };
        System.out.println("Teise isiku vanus: " + table [2][1]);

//        System.out.println(table[0].length);
//
//        int [][] table2 = { {0, 0}, {1, 2, 3}, {5}};
//        System.out.println();




    }
}
