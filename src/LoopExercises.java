public class LoopExercises {
    public static void main(String[] args) {
        //Exercise 11
        //Write a while-loop for printing numbers 1 … 100 to
        // the console (each number on a separate line

//        int x = 1;
//        int upperBound = 100;
//        while(x <= upperBound) {
//           System.out.println(x++);
//        }

        //Exercise 12
        //Write a for-loop for printing numbers 1 … 100 to
        // the console (each number on a separate line).

//        for (int t = 1; t <= 100; t++) {
//            System.out.println(t);
//        }

        //Exercise 13
        //Write foreach-loop for printing numbers 1 … 10 to
        // the console (each number on a separate line).
//        int[] n = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        int[] n = new int [10];
//        for(int i = 0; i < n.length; i++) {
//            n[i] = i + 1;
//        }

//        for(int a : n) {
//            System.out.println(a);
//        }
        //Exercise 14
        //Write a for-loop for going through the numbers in the range 1 ... 100.
        // For every number, if it divides by 3, print it to the console (each number on a separate line).

//        for (int i = 1; i <= 100; i++) {
//            if (i % 3 == 0) {
//                System.out.println(i);
//            }
//        }

        //Exercise 15
        //Define an array with values "Sun", "Metsatöll", "Queen", "Metallica".
        //Print the values of the array to the console as a comma-separated list.
        // Do not add a comma after the last element. The result should be the following:
        // "Sun, Metsatöll, Queen, Metallica".

//        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
////        System.out.println("\""+String.join(", ", bands) + "\"");
//
//            String bandsText = "";
//            for (int x = 0; x < bands.length; x++ ) {
//
//                bandsText = bandsText + bands [x];
//                if (x < bands.length - 1){
//                    bandsText = bandsText + ", ";
//
//                }
//
//        }
//        System.out.println(bandsText);
//
//            StringBuilder result = new StringBuilder();
//
//            // Loop through the array elements
//            for (int i = 0; i < bands.length; i++) {
//                // Append the current element to the result
//                result.append(bands[i]);
//
//                // Add a comma if it's not the last element
//                if (i < bands.length - 1) {
//                    result.append(", ");
//
//                }
//            }
//            System.out.println(result.toString());

        //Exercise 16
        //Do the same as you did in the previous exercise, but in the reverse order.

//        bandsText = "";
//        for (int x = bands.length - 1; x >= 0; x-- ) {
//
//            bandsText = bandsText + bands [x];
//            if (x > 0) {
//                bandsText = bandsText + ", ";
//            }
//            System.out.println(bandsText);


        //Exercise 17
        //Develop a Java program, which could accept numbers as command-line arguments in the range 0 - 9 and prints them to the console in the text-form.
        //Example: arguments 4 5 6, output: four, five, six.


        //Exercise 19: Create Patterns with For-Loops

//        for (int row = 0; row < 8; row++) {
//            for (int column = 0; column < 19; column++) {
//                if (row % 2 == 0) {
//                    if (column % 2 == 0) {
//
//                        System.out.print("#");
//                    } else {
//                        System.out.print("+");
//                    }
//                } else {
//                    if (column % 2 == 0) {
//                        System.out.print("+");
//                    } else {
//                        System.out.print("#");
//                    }
//                }
//            }
//            System.out.println(" rida: " + row);
//        }
        //Pattern 3
        /*Pattern 3:
        #
        ##
        ###
        ####
        #####
        ######
         */

        for (int row = 0; row < 6; row++) {
            for (int column = 0; column < 6; column++) {
                if(column <= row) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }
    }
}


















