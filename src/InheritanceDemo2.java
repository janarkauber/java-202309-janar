package ee.bcs.programming;

public class InheritanceDemo2 {

    public static void main(String[] args) {

        InheritanceDemo.Opel car1 = new InheritanceDemo.Opel("Opel", "Astra");
//        ((InheritanceDemo.Opel)car1).getFrontWheelRadius()
        System.out.println(car1.getFrontWheelRadius());
        System.out.println(car1.getManufacturer());
        car1.setManufacturer("Ford");
        car1.setModel("Focus");

        InheritanceDemo.FourWheeled fourWheeledCar = car1;

        InheritanceDemo.Product book = new InheritanceDemo.Product("Rehepapp", 10);
        // Üks raamat osteti ära
        book.setCount(book.getCount() - 1);
    }
}

