package bank;

public class Account {
    private final String firstName;
    private final String lastName;
    private final String accountNumber;
    private double balance;

    public Account(String accountCsv) {
        // "Kimberly, Carson, 321811857, 657" -->
        // firstName: "Kimberly", lastName: "Carson", accountNumber: "321811857", balance: 657.0
        String[] props = accountCsv.split(", ");
        this.firstName = props[0];
        this.lastName = props[1];
        this.accountNumber = props[2];
        this.balance = Double.parseDouble(props[3]);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}

