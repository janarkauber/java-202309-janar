package bank;

public class TransferResult {
    private boolean success;
    private String message;
    private Account fromAccount;
    private Account toAccount;

    public TransferResult(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public TransferResult(boolean success, String message, Account fromAccount, Account toAccount) {
        this.success = success;
        this.message = message;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    @Override
    public String toString() {
        return "TransferResult{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", fromAccount=" + fromAccount +
                ", toAccount=" + toAccount +
                '}';
    }
}

