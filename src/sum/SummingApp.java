package sum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class SummingApp {

    public static void main(String[] args) throws IOException {
        List<String> numbers = Files.readAllLines(Paths.get("nums.txt"));
        int numbersLength = numbers.get(0).length();

        String result = "";
        int reminder = 0;
        for (int digitIndex = numbersLength - 1; digitIndex >= 0; digitIndex--) {
            int sum = 0;
            for (int numberIndex = 0; numberIndex < numbers.size(); numberIndex++) {
                sum = sum + Integer.parseInt("" + numbers.get(numberIndex).charAt(digitIndex));
            }
            sum = sum + reminder;
            int digit = sum % 10;
            reminder = sum / 10;
            result = digit + result;
        }
        result = reminder + result;

        System.out.println(result);
    }
}

