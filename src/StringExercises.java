import java.text.NumberFormat;
import java.util.Locale;

public class StringExercises {
    private static String Rehepapp;

    public static void main(String[] args) {
//        String helloWorld = "Hello, World!";
//        System.out.println(helloWorld);
//
//        String helloWorld2 = "Hello, \"World\"!";
//        System.out.println(helloWorld2);
//
//        String hawkingQuote = """
//                Life would be tragic if it weren't funny"
//                """;
//        System.out.println(hawkingQuote);
//
//        String complexText = "Kui liita kokku sõnad \"See on teksti esimene pool\" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool " +
//                "See on teksti teine pool\"";
//        System.out.println(complexText);
//
//        System.out.println("Elu on ilus.");
//        System.out.println("Elu on 'ilus'");
//        System.out.println("Elu on \"ilus\".");
//
//        String confusion = "Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.";
//        System.out.println(confusion);
//        // c:\minu\kataloog\fail.txt
//        String filePath = "c:\\minu\\kataloog\\fail.txt\".";
//        System.out.println(filePath);
//
//        String beautifulSentence = """
//                Eesti keele kõige ilusam lause on: "Sõida tasa üle silla!"
//                """;
//        System.out.println(beautifulSentence);
//
//        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");
//        char a = '\'';
//
//        String name1 = "Albert";
//        String name2 = "Triinu";
//        String name3 = "Mari";
//        String winners = "Viktoriini võitjad: " + name1 + ", " + name2 + ", " + name3 + ".";
//        System.out.println(winners);
////
//        String winners2 = "Viktoriini võitjad: %s, %s, %s"
//                .formatted(name1, name2, name3);
//        System.out.println(winners2);

        //Exercise 2 String Concatenation (print to console):
        int population = 450_000;
        System.out.println("See on number: %s".formatted(population));
        System.out.println("See on number: %d".formatted(population));


        String populationOfTallinn = "450 000";
        String text1 = "Tallinnas elab %s inimest.".formatted(populationOfTallinn);
        System.out.println(text1);


        int tallinnPopulation = 450_000;
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");

        NumberFormat nf = NumberFormat.getInstance(new Locale("en"));

        //Exercise 3 String Concatenation (print to console):

        String string = "";
        String bookTitle = "Rehepapp" ;
        System.out.println( "Raamatu " + bookTitle + " autor on Andrus Kivirähk");

        //Exercise 4 String Concatenation (print to console):

        String planet1 = "Merkuur"; // ctrl+D
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        String planetcount = "8";
        System.out.println(  planet1 + ","  + planet2 + "," + planet3 + ","   + planet4 + ","  + planet5 + ","  + planet6 + ","  + planet7 + ","
                 + planet8 + "on Päikesesüsteemi " + planetcount + " planeeti" );

        // Stringi meetodid ja ehk funktsioonid

        String hello = "Hello";
        System.out.println(hello.concat(  ", world"));
        System.out.println(hello + ", world");
        System.out.println(hello.substring(1,3));
        System.out.println(hello.charAt(4));
             System.out.println((hello =="Hello")); //nii ei tohi
        System.out.println(hello.equals("Hello")); //nii on õige
        System.out.println(hello.equalsIgnoreCase( "hello"));
        System.out.println("Rida1\nRida2");
        System.out.println("Text1\tText2");
        System.out.println("Text1: \"Text2\"");




    }

}
