import java.sql.Array;
import java.util.Arrays;

public class ArrayExercises {
    public static void main(String[] args) {
        // Exercise 7
//        int[] numbers = new int[5];
//        numbers[0] = 1;
//        numbers[1] = 2;
//        numbers[2] = 3;
//        numbers[3] = 4;
//        numbers[4] = 5;
//
//        numbers = new int[]{1, 2, 3, 4, 5};
//        System.out.println(numbers[0]);
//        System.out.println(numbers[2]);
//        System.out.println(numbers[4]);
//        System.out.println(numbers[numbers.length - 1]);

        // Exercise 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println(cities[1]);
        System.out.println(Arrays.deepToString(cities));

        // Exercise 9
        //○	Element 1: 1, 2, 3
        //○	Element 2: 4, 5, 6
        //○	Element 3: 7, 8, 9, 0

        double[][] mydoubles = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0 }
        };
        System.out.println(Arrays.deepToString(mydoubles));

        // Exercise 10
        //●	Define a two-dimensional array for holding the following cities by countries:
        //○	Tallinn, Tartu, Valga, Võru
        //○	Stockholm, Uppsala, Lund, Köping
        //○	Helsinki, Espoo, Hanko, Jämsä
        //●	Use two different assignation techniques:
        //○	one-by-one
        //○	inline

        double[][] countries;







    }
}
