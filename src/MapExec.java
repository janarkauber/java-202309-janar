import java.util.HashMap;
import java.util.Map;

public class MapExec {
    public static void main(String[] args) {

        String text = "tere Tere";
        // Map<String, Integer>
        // Map<Character, Integer>
        // 't' -> 1
        // 'e' -> 2
        // 'r' -> 1
        // ' ' -> 1
        Map<String, Integer> charMap = new HashMap<>();

        for (int i = 0; i < text.length(); i++) {
            String c = "" + text.charAt(i);
            c = c.toLowerCase();
            if (charMap.get(c) == null){
                charMap.put(c, 0);

            }
            int charCount = charMap.get(c) + 1;
            charMap.put(c, charCount);
        }
        System.out.println(charMap);
    }
}
