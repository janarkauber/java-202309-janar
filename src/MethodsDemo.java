public class MethodsDemo {
    public static void main(String[] args) {
        double bmwPrice = addVat( 45_000);
        double ferrariPrice = addVat( 350_000);
        System.out.println("Bmw: " + bmwPrice);
        System.out.println("Ferrari: " + ferrariPrice);


    }

    private static double addVat(double price) {
        return 1.2 * price;

    }
}
