package estoniastatistics;

public class AdministrativeUnit {
    private String name;
    private int population;
    private boolean isCounty;

    public AdministrativeUnit(String unitText) { // " ..Harku vald|16836"
//        "aaa,bbb,ccc".split(","); --> Massiiv { "aaa", "bbb", "ccc" }
        String[] textParts = unitText.split("\\|"); // String[2] { "..Harku vald|16836" }

//        this.name = textParts[0];
        if (textParts[0].startsWith("....")) {
            this.name = textParts[0].substring(4);
        } else if (textParts[0].startsWith("..")) {
            this.name = textParts[0].substring(2);
        } else {
            this.name = textParts[0];
            this.isCounty = true;
            
        }
        this.population = Integer.parseInt(textParts[1]);
    }


    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    public boolean isCounty() {
        return isCounty;
    }

    @Override
    public String toString() {
        // Nimi: Anija vald
        // Rahvaarv: 5435
        // Tüüp: kohalik omavalitsus / maakond
        String unitType = this.isCounty ? "maakond" : "omavalitsus";
        return """
                ---
                Nimi: %s
                Rahvaarv. %s
                Haldusüksuse tüüp: %s
                """.formatted(this.name, this.population, unitType);
    }
}
