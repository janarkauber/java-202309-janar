public class ObjectExample {

    public static Person person1 = null;

    public static void main(String[] args) {
//        ObjectDemo od = new ObjectDemo();
//        ObjectDemo od2 = new ObjectDemo();
//        ObjectDemo od3 = new ObjectDemo();
//        ObjectDemo od4 = new ObjectDemo();

        person1 = new Person();
        person1.firstName = "Marek";
        person1.lastName = "Lints";
        person1.age = 150;

        Person.country ="Estonia";
        Person.country ="USA";

        Person person2 = new Person();
        person2.firstName = "Donald";
        person2.lastName = "Trump";
        person2.age = 345;

        person2.printPersonDetails();
        person1.printPersonDetails();
    }
}
