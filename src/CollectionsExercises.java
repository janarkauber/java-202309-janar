import java.util.*;

public class CollectionsExercises {
    public static void main(String[] args) {
        // Exercise 20: List
        //Defineeri a List (ArrayList) for holding texts.
        //Insert 5 random cities to the collection.
        //Print the following element to the console:
        //First
        //Third
        //Last

//        List<String> cities = new ArrayList<>();
//        cities.add("Tallinn");
//        cities.add("Tartu");
//        cities.add("Võru");
//        cities.add("Valga");
//        cities.add("Helsinki");
//        System.out.println("Esimene linn: " + cities.get(0));
//        System.out.println("Kolmas linn: " + cities.get(2));
//        System.out.println("Viimane linn: " + cities.get(4));

        //Exercise 21: Set
        //Define a Set (TreeSet) for holding texts.
        //Print the contents of the set to the console, by using the set’s forEach()-method.

//        Set<Integer> myNumbers = new TreeSet<>();
//        myNumbers.add(2);
//        myNumbers.add(6);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(34);
//        myNumbers.add(45);
//        myNumbers.forEach(tmpNum -> System.out.println(tmpNum));
//        Set<Integer> myNumber = Set.of(2, 6, 7, 7, 7, 7, 34, 45);
//        HashMap<Object, Object> myNumbers;
//        myNumbers.forEach(tmpNum -> System.out.println(tmpNum));

        //Exercise 22: Map
        //Define a Map (HashMap) with keys as texts and values as arrays of texts.
        //Define the following structure in the map:
        //Estonia -> Tallinn, Tartu, Valga, Võru
        //Sweden -> Stockholm, Uppsala, Lund, Köping
        //Finland -> Helsinki, Espoo, Hanko, Jämsä
        //Print the structure to the console by using nested for-loops.
        //
        //The result should look like that:
        //
        //Country: Estonia
        //Cities:
        //Tallinn
        //Tartu
        //Valga
        //Võru
        //Country: Sweden
        //Cities:
        //Stockholm
        //Uppsala
        //Lund
        //Köping
        //Country: Finland
        //Cities:
        //Helsinki
        //Espoo
        //Hanko
        //Jämsä

//        Map<String, List<String>> countryCitiesMap = new HashMap<>();
//        String eesti = "Eesti";
//        List <String> eestilinnad = List.of("Tallinn", "Tartu", "Valga", "Võru");
//        countryCitiesMap.put(eesti, eestilinnad);
//        countryCitiesMap.put("Rootsi", List.of("Stockholm", "Lund", "Uppsala", "Köping"));
//
        Map<String, List<String>> countryCitiesMap = Map.of(
                "Eesti", List.of("Tallinn", "Tartu", "Valga", "Võru"),
                "Rootsi", List.of("Stockholm", "Lund", "Uppsala", "Köping"),
                "Soome", List.of("Helsinki", "Espoo", "Hanko", "Jämsä")
        );
        System.out.println(countryCitiesMap);

        // Riik: Eesti
        //Riik : Soome
        //Riik : Rootsi

        Set<String> countries = countryCitiesMap.keySet();
        for (String country : countries) {
            System.out.println("Riik:" + country);
            System.out.println("Linnad:");
//            System.out.println(countryCitiesMap.get(country));
            List<String> countryCities = countryCitiesMap.get(country);
            for (String city :countryCities ){
                System.out.println("\t" + city);

            }
        }


    }
}

