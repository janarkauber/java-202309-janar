import java.rmi.MarshalledObject;
import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {

        List<String> myList = new ArrayList<>();
        System.out.println(myList);
        System.out.println(myList.size());
        myList.add("Margus");
        myList.add("Teet");
        myList.add("Riina");
        System.out.println(myList);
        System.out.println(myList.size());

        Set<String> setOfNames;
        setOfNames = new HashSet<>();
        setOfNames.add("Margus");
        setOfNames.add("Teet");
        setOfNames.add("Riina");
        System.out.println(myList);
        System.out.println(myList.size());

        Map<String, String> mapOfPeople = new HashMap<>();
        mapOfPeople.put("Mati", "Sillamäe");
        mapOfPeople.put("Tiina", "Kohila");
        mapOfPeople.put("Liina", "Tartu");
        System.out.println(mapOfPeople);
        System.out.println("Tiina elukoht: " + mapOfPeople.get("Tiina"));
        mapOfPeople.put("Tiina", "Tallinn");
        System.out.println("Tiina elukoht: " + mapOfPeople.get("Tiina"));
        System.out.println("Meie töötajad: ");
        System.out.println(mapOfPeople.keySet());
        System.out.println("Meie töötajad elavad järgmistes linnades:");
        System.out.println(mapOfPeople.values());





    }
}
