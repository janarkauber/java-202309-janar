public class MethodExercises {

    public static void main(String[] args) {

        System.out.println("Exercise 1: " + test(5));
        test2("aa", "AT");

        // Exercise 6
        System.out.println("Inimese sugu: " + extractPersonGender("37605030299"));

//        System.out.println();
////        System.out.println(test(5, 6));
//        System.out.println(true);
//        System.out.println("Tere");
//        System.out.println(3.13);
//        System.out.println(567);
//        System.out.println('r');

        System.out.println(extractPersonBirthYear("78901234215"));
        System.out.println(isPersonalCodeValid("34501234215"));
        System.out.println(isPersonalCodeValid("00000000000"));
    }

    // Exercise 1
    public static boolean test(int inputNumber) {
        return true;
    }

    public static int test(int a, int b) {
        return 2;
    }

    // Exercise 2
    public static void test2(String s1, String s2) {

    }

    // Exercise 6
    public static char extractPersonGender(String personalCode) {
        // "37605030299"
        // paaritu - mees
        // paaris - naine
        // charAt()
        // substring()

        // Võta isikukoodi esimene tähemärk
        char firstDigitChar = personalCode.charAt(0);
        int firstDigit = Integer.parseInt("" + firstDigitChar);

//        Variant 1
//        return switch(firstDigit) {
//            case 1, 3, 5, 7 -> 'M';
//            default -> 'N';
//        };

//        // Variant 2
//        if (firstDigit == 1 || firstDigit == 3 || firstDigit == 5 || firstDigit == 7) {
//            return 'M';
//        } else {
//            return 'N';
//        }

        // Variant 3
        return firstDigit % 2 == 0 ? 'N' : 'M';
    }

    public static boolean myFunEx4(int param1, int param2, boolean param3) {
        return true;
    }

    public static void printHello() {
        System.out.println("Tere");
    }

    public static int extractPersonBirthYear(String personalCode) {
        // "34501234215" --> 1945
        // Sajandid: 1, 2 --> 19. sajand
        // Sajandid: 3, 4 --> 20. sajand
        // Sajandid: 5, 6 --> 21. sajand
        // Sajandid: 7, 8 --> 22. sajand

        String birthYearInCentury = personalCode.substring(1, 3); // "45"
        int birthYearInt = Integer.parseInt(birthYearInCentury); // 45

        String firstDigit = personalCode.substring(0, 1); // "3"
        int firstDigitInt = Integer.parseInt(firstDigit); // 3
//        return switch (firstDigitInt) {
//            case 1, 2 -> 1800 + birthYearInt;
//            case 3, 4 -> 1900 + birthYearInt;
//            case 5, 6 -> 2000 + birthYearInt;
//            case 7, 8 -> 2100 + birthYearInt;
//            default -> -1;
//        };

        switch (firstDigitInt) {
            case 1:
            case 2:
                return 1800 + birthYearInt;
            case 3:
            case 4:
                return 1900 + birthYearInt;
            case 5:
            case 6:
                return 2000 + birthYearInt;
            case 7:
            case 8:
                return 2100 + birthYearInt;
            default:
                return -1;
        }
    }

    public static boolean isPersonalCodeValid(String personalCode) { // "34501234215"
        if (personalCode == null || personalCode.length() != 11 || personalCode.equals("00000000000")) {
            return false;
        }

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

        int sum = 0;
        for (int i = 0; i < weights1.length; i++) {
            sum = sum + weights1[i] * Integer.parseInt("" + personalCode.charAt(i));
        }

        int checkNumber = sum % 11;
        if (checkNumber < 10) {
            return checkNumber == Integer.parseInt("" + personalCode.charAt(10));
        }

        int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};

        sum = 0;
        for (int i = 0; i < weights2.length; i++) {
            sum = sum + weights2[i] * Integer.parseInt("" + personalCode.charAt(i));
        }

        if (checkNumber < 10) {
            return checkNumber == Integer.parseInt("" + personalCode.charAt(10));
        } else {
            return 0 == Integer.parseInt("" + personalCode.charAt(10));
        }
    }
}

