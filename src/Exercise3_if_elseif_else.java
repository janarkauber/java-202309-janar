public class Exercise3_if_elseif_else {
        public static void main(String[] args) {
            int grade = 3;  // You can change this value to test different grades

            if (grade == 1) {
                System.out.println("failed");
            } else if (grade == 2) {
                System.out.println("not passable");
            } else if (grade == 3) {
                System.out.println("passable");
            } else if (grade == 4) {
                System.out.println("good");
            } else if (grade == 5) {
                System.out.println("very good");
            } else {
                System.out.println("Invalid grade");
            }
        }


}
