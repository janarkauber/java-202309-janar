package visitors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class VisitApp {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Külastajatefaili asukoht määramata!");
            return;
        }
        List<String> visitLines = readFileLines(args[0]);
        List<VisitCount> visitCounts = convertToVisitCountList(visitLines);

        // Variant 1 ja 2
//        visitCounts.sort(new VisitCountComparator());

        // Variant 3
//        visitCounts.sort((c1, c2) -> c1.getCount() - c2.getCount());

        // Variant 4
        visitCounts.sort(Comparator.comparingInt(VisitCount::getCount));

        System.out.println(visitCounts);
        System.out.println("Kõige rohkem külastajaid oli järgmisel kuupäeval: " +
                visitCounts.get(visitCounts.size() - 1).getDate());

    }

    private static List<String> readFileLines(String path) throws IOException {
        return Files.readAllLines(Paths.get(path));
    }

    private static List<VisitCount> convertToVisitCountList(List<String> visitLines) {
        List<VisitCount> visitCounts = new ArrayList<>();
        for (String visitLine : visitLines) {
            visitCounts.add(new VisitCount(visitLine));
        }
        return visitCounts;
    }

    static class VisitCount {
        private String date;
        private int count;

        public String getDate() {
            return date;
        }

        public int getCount() {
            return count;
        }

        public VisitCount(String visitLine) {
            String[] lineParts = visitLine.split(", ");
            date = lineParts[0];
            count = Integer.parseInt(lineParts[1]);
        }

        @Override
        public String toString() {
            return "Kuupäev: %s, külastajaid: %s\n".formatted(date, count);
        }
    }

    static class VisitCountComparator implements Comparator<VisitCount> {

        @Override
        public int compare(VisitCount visitCount1, VisitCount visitCount2) {
            // Variant 1
//            if (visitCount1.getCount() < visitCount2.getCount()) {
//                return -1;
//            } else if (visitCount1.getCount() == visitCount2.getCount()) {
//                return 0;
//            } else {
//                return 1;
//            }

            // Variant 2
            return visitCount1.getCount() - visitCount2.getCount();
        }
    }
}

