public class Person {

    public String firstName;
    public String lastName;
    public int age;

    public static String country; // Valesti defineeritud objektimuutuja

    public void printPersonDetails() {
        System.out.println("Eesnimi:" + firstName);
        System.out.println("Perenimi:" + lastName);
        System.out.println("Vanus:" + age);
    }
}

