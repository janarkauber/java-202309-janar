public class Book {
    public String title;
    public int pages;
    public int publishedYear;

    public Book(String title, int pages, int publishedYear) {
        this.title = title;
        this.pages = pages;
        this.publishedYear = publishedYear;
    }

    public Book() {

    }

    public Book (String title) {
        this.title = "Maailma Atlas";
    }
}

