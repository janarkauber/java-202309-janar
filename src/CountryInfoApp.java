import javax.sound.midi.Soundbank;
import java.util.List;

public class CountryInfoApp {

    public static void main(String[] args) {
        CountryInfo estonia = new CountryInfo();
        estonia.name = "Eesti";
        estonia.cities = List.of("Tallinn", "Tartu", "Valga", "Võru");
//        estonia.cities = new String[]{"Tallinn", "Tartu", "Valga", "Võru"};

        CountryInfo sweden = new CountryInfo("Rootsi", List.of("Stockholm", "Uppsala", "Lund", "Köping"));
        CountryInfo finland = new CountryInfo("Soome", List.of("Helsinki", "Espoo", "Hanko", "Jämsä"));

//        System.out.println("Riik: " + sweden.name);
//        System.out.println("Linnad:");
//        for(String city : sweden.cities) {
//            System.out.println("    " + city);
//        }
//
////        CountryInfo[] countries = { estonia, sweden, finland };
//        CountryInfo[] countries = new CountryInfo[3];
//        countries[0] = estonia;
//        countries[1] = sweden;
//        countries[2] = finland;
//
//        for (CountryInfo country : countries) {
//            System.out.println("Riik: " + country.name);
//            System.out.println("Linnad:");
//            for (String city : country.cities) {
//                System.out.println("    " + city);
//            }
//            System.out.println();
//        }

//        estonia.printOut();
//        sweden.printOut();
//        finland.printOut();

        System.out.println(estonia);

    }
}

