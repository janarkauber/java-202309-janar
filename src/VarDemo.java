public class VarDemo {

    public static void main(String[] args) {
        int myNumber = 3;
        System.out.println(myNumber * 2);
        myNumber = myNumber + 55;
        System.out.println(myNumber);

        float myDecimal = 3.14f;
        double myDecimal2 = 3.14d;
        myDecimal2  = myDecimal;

        boolean myBoolValue = false;
        System.out.print(myBoolValue);
        char myLetter = 'A';
        char myLetter2 = 'Ö';
        char myNumberChar = 65;
        System.out.println(myNumberChar);

    }
}

