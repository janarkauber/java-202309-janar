function displayResult(boxId, result) {
    const resultBox = document.querySelector(`#${boxId}`);
    if (Array.isArray(result)) {
        resultBox.textContent = result.join(', ');
    } else {
        resultBox.textContent = result;
    }
}
